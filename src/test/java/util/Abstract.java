package util;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by satriditya on 1810//16.
 */
public class Abstract {
        protected AndroidDriver androidDriver;
        protected Mapping androidMap = new Mapping("/Users/satriditya/IdeaProjects/friendspot/src/test/java/util/friendspot.properties");
        protected int runId = 972;
        protected String testResult;
        protected int testStatus;
        protected int testId;
        protected String device1 = "Nexus 5";
        protected String device2 = "satriditya - Redmi 3 Pro";

        @BeforeMethod
        public void setUp() throws Exception {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("appium-version", "1.5.2");
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("platformVersion", "6.0.1");
            capabilities.setCapability("appPackage", "com.icehousecorp.friendspot");
            capabilities.setCapability("deviceName", device1);
            androidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
            androidDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        }

        protected void scrollDown(String element) throws Exception {
//      int x = (int) (androidDriver.findElement(androidMap.getLocator(element)).getLocation().getX() + (androidDriver.manage().window().getSize().getWidth() * 0.5));
            int x = (int) (androidDriver.findElement(androidMap.getLocator(element)).getSize().getWidth() * 0.5);
            int startY = (int) (androidDriver.findElement(androidMap.getLocator(element)).getSize().getHeight() * 0.9);
            int endY = (int) (androidDriver.findElement(androidMap.getLocator(element)).getSize().getHeight() * 0.1);
            androidDriver.swipe(x, startY, x, endY, (int) 2000);
        }

        @AfterMethod
        public void tearDown() throws Exception {
            Thread.sleep(5000);
            androidDriver.quit();
        }

}
