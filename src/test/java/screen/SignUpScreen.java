package screen;

import enums.User;
import io.appium.java_client.android.AndroidDriver;
import util.Abstract;

/**
 * Created by satriditya on 1910//16.
 */
public class SignUpScreen extends Abstract {
    public SignUpScreen(AndroidDriver androidDriver)
    {
        this.androidDriver=androidDriver;
    }

    public void signup_assertion() throws Exception {
        if ((androidDriver.findElement(androidMap.getLocator("signupassertion")).getText().equals("Do you have an account?"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }

    public void assertEmailEmpty2() throws Exception {
        if ((androidDriver.findElement(androidMap.getLocator("emailempty2")).getText().equals("Email is empty"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }

    public void assertPasswordEmpty2() throws Exception {
        if ((androidDriver.findElement(androidMap.getLocator("passwordempty2")).getText().equals("Password is empty"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }

    /*public void input_username() throws Exception
    {
        androidDriver.findElement(androidMap.getLocator("signup_username")).click();
        androidDriver.findElement(androidMap.getLocator("signup_username")).clear();
        androidDriver.findElement(androidMap.getLocator("signup_username")).sendKeys("DDTTEST");
        androidDriver.hideKeyboard();
    }*/

    public void signup_input_email(User user) throws Exception {
        androidDriver.findElement(androidMap.getLocator("signup_email")).click();
        androidDriver.findElement(androidMap.getLocator("signup_email")).clear();
        androidDriver.findElement(androidMap.getLocator("signup_email")).sendKeys(user.getEmail());
        androidDriver.hideKeyboard();
    }

    public void signup_input_password(User user) throws Exception {
        androidDriver.findElement(androidMap.getLocator("signup_password")).click();
        androidDriver.findElement(androidMap.getLocator("signup_password")).clear();
        androidDriver.findElement(androidMap.getLocator("signup_password")).sendKeys(user.getPassword());
        androidDriver.hideKeyboard();
    }

    public void signup() throws Exception {
        androidDriver.findElement(androidMap.getLocator("to_signup")).click();
    }

    public void login_from_signup() throws Exception {
        Thread.sleep(5000);
        scrollDown("signupopt");
        androidDriver.findElement(androidMap.getLocator("to_login")).click();
    }

    public void signup_facebook() throws Exception {
        Thread.sleep(5000);
        androidDriver.findElement(androidMap.getLocator("fbsignup")).click();
    }

    public void signup_google_1() throws Exception {
        Thread.sleep(5000);
        androidDriver.findElement(androidMap.getLocator("googlesignup")).click();
        androidDriver.findElement(androidMap.getLocator("googleaccount1")).click();
    }
}
