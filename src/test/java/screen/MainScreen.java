package screen;

import io.appium.java_client.android.AndroidDriver;
import util.Abstract;

/**
 * Created by satriditya on 28/10/16.
 */
public class MainScreen extends Abstract
{
    public MainScreen(AndroidDriver androidDriver)
    {
        this.androidDriver=androidDriver;
    }

    public void assertMainScreen(/*String device*/) throws Exception {
        Thread.sleep(5000);
        // if (device==device1){
            if ((androidDriver.findElement(androidMap.getLocator("mainassertion")).getText().equals("  Friend's around you"))) {
                System.out.print("Benar");
            } else {
                throw new Exception("Salah");
            }
        // }else if(device==device2){
        }

    public void goto_threedots() throws Exception {
        androidDriver.findElement(androidMap.getLocator("goto_threedots")).click();
    }

    public void goto_userprofile() throws Exception {
        // androidDriver.findElement(androidMap.getLocator("threedotsbutton")).click();
        androidDriver.findElement(androidMap.getLocator("goto_userprofile")).click();
    }

    public void goto_notification() throws Exception {
        // androidDriver.findElement(androidMap.getLocator("threedotsbutton")).click();
        androidDriver.findElement(androidMap.getLocator("goto_notif")).click();
    }

    public void goto_friend() throws Exception {
        Thread.sleep(10000);
        androidDriver.findElement(androidMap.getLocator("gotofriend")).click();
    }

    public void goto_chat() throws Exception  {
        Thread.sleep(10000);
        androidDriver.findElement(androidMap.getLocator("gotochat")).click();
    }
}
