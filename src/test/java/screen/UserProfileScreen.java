package screen;

import io.appium.java_client.android.AndroidDriver;
import sun.awt.windows.ThemeReader;
import util.Abstract;

/**
 * Created by satriditya on 28/10/16.
 */

public class UserProfileScreen extends Abstract {
    public UserProfileScreen(AndroidDriver androidDriver)
    {
        this.androidDriver=androidDriver;
    }

    public void assert_userprofile() throws Exception {
        if ((androidDriver.findElement(androidMap.getLocator("userprofileassertion")).getText().equals("User Profile"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }

    public void assert_notification() throws Exception {
        if ((androidDriver.findElement(androidMap.getLocator("notifassertion")).getText().equals("Notifications"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }

    public void input_name() throws Exception  {
        Thread.sleep(5000);
        androidDriver.hideKeyboard();
        androidDriver.findElement(androidMap.getLocator("name")).clear();
        androidDriver.findElement(androidMap.getLocator("name")).click();
        androidDriver.findElement(androidMap.getLocator("name")).sendKeys("Aditya Satriady");
        androidDriver.hideKeyboard();
    }

    /* public void input_birthdate() throws Exception  {
        androidDriver.findElement(androidMap.getLocator("birthdate")).clear();
        androidDriver.findElement(androidMap.getLocator("birthdate")).click();
        androidDriver.findElement(androidMap.getLocator("birthdate")).getText("Aditya Satriady");
        androidDriver.hideKeyboard();   } */

    public void input_phonenumber() throws Exception {
        androidDriver.hideKeyboard();
        androidDriver.findElement(androidMap.getLocator("phonenumber")).click();
        androidDriver.findElement(androidMap.getLocator("phonenumber")).clear();
        androidDriver.findElement(androidMap.getLocator("phonenumber")).sendKeys("081223956415");
        androidDriver.hideKeyboard();
    }

    public void click_savechanges() throws Exception {
        androidDriver.hideKeyboard();
        androidDriver.findElement(androidMap.getLocator("savechanges")).click();
    }

    public void logout() throws Exception {
        Thread.sleep(5000);
        androidDriver.findElement(androidMap.getLocator("logout")).click();
        androidDriver.findElement(androidMap.getLocator("logoutyes")).click();
    }

    /* public void change_avatar() throws Exception {
        Thread.sleep(5000)  ;
        androidDriver.findElement(androidMap.getLocator("photo")).click();
    } */

    public void back_from_userprofile() throws Exception {
        Thread.sleep(5000);
        androidDriver.findElement(androidMap.getLocator("back")).click();
    }
}
