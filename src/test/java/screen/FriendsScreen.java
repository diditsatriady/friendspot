package screen;

import io.appium.java_client.android.AndroidDriver;
import util.Abstract;

/**
 * Created by satriditya on 1/11/16.
 */
public class FriendsScreen extends Abstract {
    public FriendsScreen(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    public void assert_friendscreen() throws Exception  {
        if ((androidDriver.findElement(androidMap.getLocator("friendassertion")).getText().equals("My Friends"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }

    public void click_friendrequest() throws Exception  {
        Thread.sleep(10000);
        androidDriver.findElement(androidMap.getLocator("friendrequest")).click();
    }

    public void click_addfriend() throws Exception  {
        Thread.sleep(10000);
        androidDriver.findElement(androidMap.getLocator("addfriend")).click();
    }

    public void assert_friendreq() throws Exception {
        if ((androidDriver.findElement(androidMap.getLocator("friendreqassertion")).getText().equals("Friend Request"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }

    public void assert_addfriend() throws Exception {
        if ((androidDriver.findElement(androidMap.getLocator("addfriendassertion")).getText().equals("Add Friend"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }
}