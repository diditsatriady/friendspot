package screen;

import enums.User;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import util.Abstract;

/**
 * Created by satriditya on 1910//16.
 */
public class LoginScreen extends Abstract {
    public LoginScreen(AndroidDriver androidDriver)
    {
       this.androidDriver=androidDriver;
    }

    public void assertLoginScreen() throws Exception {
        if ((androidDriver.findElement(androidMap.getLocator("loginassertion")).getText().equals("Spot friend's around you"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }

    public void assertEmailEmpty() throws Exception {
        if ((androidDriver.findElement(androidMap.getLocator("emailempty")).getText().equals("Email is empty"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }

    public void assertPasswordEmpty() throws Exception {
        if ((androidDriver.findElement(androidMap.getLocator("passwordempty")).getText().equals("Password is empty"))) {
            System.out.print("Benar");
        } else {
            throw new Exception("Salah");
        }
    }

    public void input_email(User user) throws Exception {
        androidDriver.findElement(androidMap.getLocator("email")).click();
        androidDriver.findElement(androidMap.getLocator("email")).clear();
        androidDriver.findElement(androidMap.getLocator("email")).sendKeys(user.getEmail());
        androidDriver.hideKeyboard();
    }

    public void input_password(User user) throws Exception {
        androidDriver.findElement(androidMap.getLocator("password")).click();
        androidDriver.findElement(androidMap.getLocator("password")).clear();
        androidDriver.findElement(androidMap.getLocator("password")).sendKeys(user.getPassword());
        // androidDriver.hideKeyboard();
    }

    public void click_login() throws Exception {
        Thread.sleep(5000);
        // androidDriver.pressKeyCode(AndroidKeyCode.KEYCODE_BACK);
        androidDriver.hideKeyboard();
        androidDriver.findElement(androidMap.getLocator("login")).click();
    }

    public void click_signup() throws Exception {
        Thread.sleep(5000);
        androidDriver.hideKeyboard();
        scrollDown("loginopt");
        androidDriver.findElement(androidMap.getLocator("signup")).click();
    }

    public void login_facebook() throws Exception {
        Thread.sleep(5000);
        androidDriver.hideKeyboard();
        androidDriver.findElement(androidMap.getLocator("fblogin")).click();
    }

    public void login_google_1() throws Exception {
        Thread.sleep(5000);
        androidDriver.hideKeyboard();
        androidDriver.findElement(androidMap.getLocator("googlelogin")).click();
        androidDriver.findElement(androidMap.getLocator("googleaccount1")).click();
    }
}
