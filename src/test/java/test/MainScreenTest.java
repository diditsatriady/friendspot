package test;

import org.testng.annotations.Test;
import screen.FriendsScreen;
import screen.MainScreen;
import screen.UserProfileScreen;
import util.APIException;
import util.Abstract;

/**
 * Created by satriditya on 28/10/16.
 */
public class MainScreenTest extends LoginTest
{
    MainScreen main;
    UserProfileScreen user;
    FriendsScreen friend;

    @Test
    public void testMainScreenAssertion() throws Exception, APIException {
        try {
            testId = 45732;
            // testLoginValidData();
            main = new MainScreen(androidDriver);
            // main.assertMainScreen2();
            main.assertMainScreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testGoToUserProfile() throws Exception, APIException {
        try {
            testId = 45772;
            // testLoginValidData();
            Thread.sleep(8000);
            main = new MainScreen(androidDriver);
            main.goto_threedots();
            main.goto_userprofile();
            user = new UserProfileScreen(androidDriver);
            user.assert_userprofile();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }


    @Test
    public void testGoToNotification() throws Exception, APIException {
        try {
            testId = 46190;
            // testLoginValidData();
            Thread.sleep(5000);
            main = new MainScreen(androidDriver);
            main.goto_threedots();
            main.goto_notification();
            user = new UserProfileScreen(androidDriver);
            user.assert_notification();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testClickFriend() throws Exception, APIException {
        try {
            testId = 46191;
            // testLoginValidData();
            Thread.sleep(5000);
            main = new MainScreen(androidDriver);
            main.goto_friend();
            friend = new FriendsScreen(androidDriver);
            friend.assert_friendscreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus, testResult, runId, testId);
        }
    }


    @Test
    public void testClickChat() throws Exception, APIException {
        try {
            testId = 46192;
            // testLoginValidData();
            Thread.sleep(5000);
            main = new MainScreen(androidDriver);
            main.goto_chat();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus, testResult, runId, testId);
        }
    }
}
