package test;

import enums.User;
import org.testng.annotations.Test;
import screen.LoginScreen;
import screen.MainScreen;
import screen.Onboarding;
import screen.SignUpScreen;
import util.APIException;
import util.Abstract;

/**
 * Created by satriditya on 1910//16.
 */
public class LoginTest extends Abstract {
    LoginScreen login;
    MainScreen main;
    SignUpScreen signup;

    @Test
    public void testLoginScreenAssertion() throws Exception, APIException {
        try {
            testId = 45706;
            Thread.sleep(5000);
            login = new LoginScreen(androidDriver);
            login.assertLoginScreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testLoginValidData() throws Exception, APIException {
        try {
            testId = 45712;
            login = new LoginScreen(androidDriver);
            login.input_email(User.VALID);
            login.input_password(User.VALID);
            login.click_login();
            main = new MainScreen(androidDriver);
            main.assertMainScreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testLoginInvalidEmail() throws Exception, APIException {
        try {
            testId = 45710;
            login = new LoginScreen(androidDriver);
            login.input_email(User.INVALID);
            login.input_password(User.VALID);
            login.click_login();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testLoginIncorrectPassword() throws Exception, APIException {
        try {
            testId = 45711;
            login = new LoginScreen(androidDriver);
            login.input_email(User.VALID);
            login.input_password(User.INVALID);
            login.click_login();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testLoginEmptyData() throws Exception, APIException {
        try {
            testId = 45707;
            login = new LoginScreen(androidDriver);
            login.click_login();
            login.assertEmailEmpty();
            login.assertPasswordEmpty();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testClickSignUp() throws Exception, APIException {
        try {
            testId = 45719;
            login = new LoginScreen(androidDriver);
            login.click_signup();
            androidDriver.hideKeyboard();
            signup = new SignUpScreen(androidDriver);
            signup.signup_assertion();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus, testResult, runId, testId);
        }
    }

    @Test
    public void testLoggedInFacebook() throws Exception, APIException {
        try {
            testId = 45714;
            login = new LoginScreen(androidDriver);
            login.login_facebook();
            main = new MainScreen(androidDriver);
            main.assertMainScreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testLoggedInGoogle() throws Exception, APIException {
        try {
            testId = 45717;
            login = new LoginScreen(androidDriver);
            login.login_google_1();
            main = new MainScreen(androidDriver);
            main.assertMainScreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }
}
