package test;

import enums.User;
import org.testng.annotations.Test;
import screen.LoginScreen;
import screen.MainScreen;
import screen.SignUpScreen;
import util.APIException;
import util.Abstract;

/**
 * Created by satriditya on 1910//16.
 */
public class SignUpTest extends LoginTest {
    SignUpScreen signup;
    MainScreen main;
    LoginScreen login;

    @Test
    public void testSignUpAssertion() throws Exception, APIException {
        try {
            testClickSignUp();
            testId = 45725;
            signup = new SignUpScreen(androidDriver);
            signup.signup_assertion();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testSignUpValidData() throws Exception, APIException {
        try {
            testClickSignUp();
            testId = 45729;
            signup = new SignUpScreen(androidDriver);
            signup.signup_input_email(User.SUDIRO);
            signup.signup_input_password(User.SUDIRO);
            signup.signup();
            main = new MainScreen(androidDriver);
            main.assertMainScreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testSignUpEmptyData() throws Exception, APIException {
        try {
            testClickSignUp();
            testId = 45726;
            signup = new SignUpScreen(androidDriver);
            signup.signup();
            signup.assertEmailEmpty2();
            signup.assertPasswordEmpty2();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testClickLogin() throws Exception, APIException {
        try {
            testClickSignUp();
            testId = 45731;
            signup = new SignUpScreen(androidDriver);
            signup.login_from_signup();
            login = new LoginScreen(androidDriver);
            login.assertLoginScreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testSignedUpFacebook() throws Exception, APIException {
        try {
            testClickSignUp();
            testId = 45720;
            signup = new SignUpScreen(androidDriver);
            signup.signup_facebook();
            main = new MainScreen(androidDriver);
            main.assertMainScreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testSignedUpGoogle() throws Exception, APIException {
        try {
            testClickSignUp();
            testId = 45723;
            signup = new SignUpScreen(androidDriver);
            signup.signup_google_1();
            main = new MainScreen(androidDriver);
            main.assertMainScreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }
}
