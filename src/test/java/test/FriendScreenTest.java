package test;

import org.testng.annotations.Test;
import screen.FriendsScreen;
import sun.security.krb5.internal.APOptions;
import util.APIException;

/**
 * Created by satriditya on 9/11/16.
 */
public class FriendScreenTest extends MainScreenTest
{
    FriendsScreen friend;

    @Test
    public void testAssertFriendScreen() throws Exception, APIException {
        try {
            testClickFriend();
            testId = 45837;
            friend = new FriendsScreen(androidDriver);
            friend.assert_friendscreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }


    @Test
    public void testClickFriendRequest() throws Exception, APIException {
        try {
            testClickFriend();
            testId = 45832;
            friend = new FriendsScreen(androidDriver);
            friend.click_friendrequest();
            friend.assert_friendreq();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }


    @Test
    public void testClickAddFriend() throws Exception, APIException {
        try {
            testClickFriend();
            testId = 45833;
            friend = new FriendsScreen(androidDriver);
            friend.click_addfriend();
            friend.assert_addfriend();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }
}
