package test;

import org.testng.annotations.Test;
import screen.LoginScreen;
import screen.MainScreen;
import screen.UserProfileScreen;
import util.APIException;
import util.Abstract;

/**
 * Created by satriditya on 1/11/16.
 */
public class UserProfileTest extends MainScreenTest
{
    UserProfileScreen user;
    LoginScreen login;

    @Test
    public void testUserProfileAssertion() throws Exception, APIException {
        try {
            testGoToUserProfile();
            testId = 46182;
            androidDriver.hideKeyboard();
            user = new UserProfileScreen(androidDriver);
            user.assert_userprofile();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testFillUserName() throws Exception, APIException {
        try {
            testGoToUserProfile();
            testId = 46186;
            user = new UserProfileScreen(androidDriver);
            user.input_name();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testFillPhoneNumber() throws Exception, APIException {
        try {
            testGoToUserProfile();
            testId = 46188;
            user = new UserProfileScreen(androidDriver);
            user.input_phonenumber();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus, testResult, runId, testId);
        }

    }

    @Test
    public void testClickSaveChanges() throws Exception, APIException {
        try {
            testGoToUserProfile();
            testId = 46189;
            user = new UserProfileScreen(androidDriver);
            user.click_savechanges();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }



    @Test
    public void testLogoutYes() throws Exception, APIException {
        try {
            testGoToUserProfile();
            testId = 46183;
            user = new UserProfileScreen(androidDriver);
            user.logout();
            login = new LoginScreen(androidDriver);
            login.assertLoginScreen();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }

    @Test
    public void testBackFromUserProfile() throws Exception, APIException {
        try {
            testGoToUserProfile();
            testId = 46253;
            user = new UserProfileScreen(androidDriver);
            user.back_from_userprofile();
            testResult = "Passed";
            testStatus = 1;
        } catch (Exception e) {
            e.printStackTrace();
            testResult = "Failed";
            testStatus = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(testStatus,testResult,runId,testId);
        }
    }
}
