package enums;

/**
 * Created by satriditya on 20/10/16.
 */
public enum User
{
    VALID("lalala@icehousecorp.com", "lalalala"),
    INVALID("unang@icehousecorp.com", "salah"),
    SALO("mikasalo@ferrari.com", "mikasalo"),
    IRVINE("eddieirvine@ferrari.com", "irvine"),
    SUDIRO("tora.sudiro@yahoo.com", "torasudiro");

    private String email;
    private String password;

    User(String email, String password)
    {
        this.email = email;
        this.password = password;
    }

    public String getEmail()
    {
        return email;
    }

    public String getPassword()
    {
        return password;
    }
}
