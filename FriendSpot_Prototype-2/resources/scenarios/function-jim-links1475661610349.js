(function(window, undefined) {

  var jimLinks = {
    "660b25cb-93fa-41d2-b8d4-721789464a4d" : {
      "Image_71" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ],
      "Image_144" : [
        "5bc698c6-ba50-4499-a768-f1c969c6d07e"
      ],
      "Flat-button-light" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ]
    },
    "3596e8ed-2b55-4dcf-b835-4c2aee7a948e" : {
      "Image_71" : [
        "711ea13d-14fd-458d-9c90-37111a7b45f1"
      ]
    },
    "711ea13d-14fd-458d-9c90-37111a7b45f1" : {
      "Picture_3" : [
        "3596e8ed-2b55-4dcf-b835-4c2aee7a948e"
      ],
      "Label_39" : [
        "660b25cb-93fa-41d2-b8d4-721789464a4d"
      ],
      "Image_36" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ],
      "Image_70" : [
        "ee912a60-8c12-411e-9677-2fd12d7c7ca9"
      ],
      "Image_71" : [
        "711ea13d-14fd-458d-9c90-37111a7b45f1"
      ],
      "Image_74" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ]
    },
    "cf28176e-5685-4c1c-9d09-aa5e847b269f" : {
      "Label_39" : [
        "660b25cb-93fa-41d2-b8d4-721789464a4d"
      ],
      "Image_71" : [
        "711ea13d-14fd-458d-9c90-37111a7b45f1"
      ],
      "Image_36" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ],
      "Image_74" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ],
      "Image_16" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ],
      "Image_77" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ]
    },
    "782f3941-0888-4e44-b59a-bec8d1a92242" : {
      "Image_71" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ],
      "Flat-button-light" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ],
      "Flat-button-light_1" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ],
      "Picture_4" : [
        "4dfbac88-daa2-4138-933a-f25bdd99c993"
      ]
    },
    "4dfbac88-daa2-4138-933a-f25bdd99c993" : {
      "Icon-hagouts_8" : [
        "3596e8ed-2b55-4dcf-b835-4c2aee7a948e"
      ],
      "Label_16" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
      "Text_1" : [
        "5bc698c6-ba50-4499-a768-f1c969c6d07e"
      ]
    },
    "474fb2ff-7941-4f4e-8b52-fbb665e7b010" : {
      "Rectangle_3" : [
        "bab033c8-7aeb-48a0-be00-85126d68bc51"
      ],
      "Float-button-light" : [
        "cf28176e-5685-4c1c-9d09-aa5e847b269f"
      ],
      "Label_39" : [
        "660b25cb-93fa-41d2-b8d4-721789464a4d"
      ],
      "Image_36" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ],
      "Image_70" : [
        "ee912a60-8c12-411e-9677-2fd12d7c7ca9"
      ],
      "Image_71" : [
        "711ea13d-14fd-458d-9c90-37111a7b45f1"
      ],
      "Image_74" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ]
    },
    "ee912a60-8c12-411e-9677-2fd12d7c7ca9" : {
      "Label_39" : [
        "660b25cb-93fa-41d2-b8d4-721789464a4d"
      ],
      "Image_36" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ],
      "Image_70" : [
        "ee912a60-8c12-411e-9677-2fd12d7c7ca9"
      ],
      "Image_71" : [
        "711ea13d-14fd-458d-9c90-37111a7b45f1"
      ],
      "Image_74" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ]
    },
    "5bc698c6-ba50-4499-a768-f1c969c6d07e" : {
      "Flat-button-light" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ],
      "Text_4" : [
        "ec5f2d3c-5977-47cd-9739-054823139e40"
      ]
    },
    "72c71c69-9e79-42f8-bcba-e66294e025e2" : {
      "Picture_5" : [
        "4dfbac88-daa2-4138-933a-f25bdd99c993"
      ],
      "Picture_4" : [
        "4dfbac88-daa2-4138-933a-f25bdd99c993"
      ],
      "Picture_6" : [
        "4dfbac88-daa2-4138-933a-f25bdd99c993"
      ],
      "Label_39" : [
        "660b25cb-93fa-41d2-b8d4-721789464a4d"
      ],
      "Image_36" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ],
      "Image_70" : [
        "ee912a60-8c12-411e-9677-2fd12d7c7ca9"
      ],
      "Image_71" : [
        "711ea13d-14fd-458d-9c90-37111a7b45f1"
      ],
      "Image_74" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ],
      "Image_77" : [
        "782f3941-0888-4e44-b59a-bec8d1a92242"
      ],
      "Image_75" : [
        "fb95e79b-cf7d-47af-82de-1e5475812f6c"
      ]
    },
    "fb95e79b-cf7d-47af-82de-1e5475812f6c" : {
      "Image_71" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ],
      "Flat-button-light" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ],
      "Flat-button-light_1" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ],
      "Flat-button-light_2" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ],
      "Flat-button-light_3" : [
        "72c71c69-9e79-42f8-bcba-e66294e025e2"
      ],
      "Picture_4" : [
        "4dfbac88-daa2-4138-933a-f25bdd99c993"
      ]
    },
    "ec5f2d3c-5977-47cd-9739-054823139e40" : {
      "Flat-button-light" : [
        "474fb2ff-7941-4f4e-8b52-fbb665e7b010"
      ],
      "Text_4" : [
        "5bc698c6-ba50-4499-a768-f1c969c6d07e"
      ]
    },
    "bab033c8-7aeb-48a0-be00-85126d68bc51" : {
      "Float-button-light" : [
        "cf28176e-5685-4c1c-9d09-aa5e847b269f"
      ],
      "Picture_5" : [
        "4dfbac88-daa2-4138-933a-f25bdd99c993"
      ],
      "Picture_6" : [
        "4dfbac88-daa2-4138-933a-f25bdd99c993"
      ],
      "Picture_7" : [
        "4dfbac88-daa2-4138-933a-f25bdd99c993"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);