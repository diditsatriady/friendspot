(function(window, undefined) {

    /*********************** START STATIC ACCESS METHODS ************************/

    jQuery.extend(jimMobile, {
        "loadScrollBars": function() {
            jQuery(".s-660b25cb-93fa-41d2-b8d4-721789464a4d .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-3596e8ed-2b55-4dcf-b835-4c2aee7a948e .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-711ea13d-14fd-458d-9c90-37111a7b45f1 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-cf28176e-5685-4c1c-9d09-aa5e847b269f .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-782f3941-0888-4e44-b59a-bec8d1a92242 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-4dfbac88-daa2-4138-933a-f25bdd99c993 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-d12245cc-1680-458d-89dd-4f0d7fb22724 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-474fb2ff-7941-4f4e-8b52-fbb665e7b010 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-ee912a60-8c12-411e-9677-2fd12d7c7ca9 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-5bc698c6-ba50-4499-a768-f1c969c6d07e .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-72c71c69-9e79-42f8-bcba-e66294e025e2 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-fb95e79b-cf7d-47af-82de-1e5475812f6c .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-ec5f2d3c-5977-47cd-9739-054823139e40 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-bab033c8-7aeb-48a0-be00-85126d68bc51 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
         }
    });

    /*********************** END STATIC ACCESS METHODS ************************/

}) (window);