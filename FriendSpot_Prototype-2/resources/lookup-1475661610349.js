(function(window, undefined) {
  var dictionary = {
    "660b25cb-93fa-41d2-b8d4-721789464a4d": "User Profile Screen",
    "3596e8ed-2b55-4dcf-b835-4c2aee7a948e": "Chat Screen",
    "711ea13d-14fd-458d-9c90-37111a7b45f1": "Inbox Screen",
    "cf28176e-5685-4c1c-9d09-aa5e847b269f": "Add Near Friend Screen",
    "782f3941-0888-4e44-b59a-bec8d1a92242": "Add Friend Screen",
    "4dfbac88-daa2-4138-933a-f25bdd99c993": "Friend Profile Screen",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Launch Screen",
    "474fb2ff-7941-4f4e-8b52-fbb665e7b010": "Home Screen",
    "ee912a60-8c12-411e-9677-2fd12d7c7ca9": "Notification Screen",
    "5bc698c6-ba50-4499-a768-f1c969c6d07e": "Login Screen",
    "72c71c69-9e79-42f8-bcba-e66294e025e2": "Friends Screen",
    "fb95e79b-cf7d-47af-82de-1e5475812f6c": "Friend Request Screen",
    "ec5f2d3c-5977-47cd-9739-054823139e40": "Signup Screen",
    "bab033c8-7aeb-48a0-be00-85126d68bc51": "Detail Near Friend Screen",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);